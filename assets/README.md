# Dan Church

Computer Software Professional

<!-- Begin: Email -->
<p class="d-none d-print-block"><a href="mailto:wadfujd4a@mozmail.com">wadfujd4a@mozmail.com</a></p>
<a href="mailto:wadfujd4a@mozmail.com" class="d-print-none"><img alt="wadfujd4a@mozmail.com" src="assets/email.svg"></a>&nbsp;
<!-- End: Email -->
<!-- Begin: Codeberg -->
<p class="d-none d-print-block">h3xx@Codeberg: <a href="https://codeberg.org/h3xx" target="_blank">codeberg.org/h3xx</a></p>
<a href="https://codeberg.org/h3xx" target="_blank" class="d-print-none"><img alt="h3xx@Codeberg" src="assets/codeberg.svg"></a>&nbsp;
<!-- End: Codeberg -->
<!-- Begin: GitHub -->
<p class="d-none d-print-block">h3xx@GitHub: <a href="https://github.com/h3xx" target="_blank">github.com/h3xx</a></p>
<a href="https://github.com/h3xx" target="_blank" class="d-print-none"><img alt="h3xx@GitHub" src="assets/github.svg"></a>&nbsp;
<!-- End: GitHub -->
<!-- Begin: LinkedIn -->
<p class="d-none d-print-block">LinkedIn: <a href="https://www.linkedin.com/in/daniel-church-04b2651b/" target="_blank">www.linkedin.com/in/daniel-church-04b2651b/</a></p>
<a href="https://www.linkedin.com/in/daniel-church-04b2651b/" target="_blank" class="d-print-none"><img alt="LinkedIn" src="assets/linkedin.svg"></a>
<!-- End: LinkedIn -->

## Summary

I'm a software engineer with over 9 years of development experience. I've lead teams of up to 4 engineers doing:
- Backend and frontend implementation, including database design and planning
- Incident response and disaster recovery
- Revamping infrastructure and cloud migration
- Technical writing and training engineers
- Project planning and estimation
- Application security review and testing

## Specialties

Software Development, AI-Assisted Software Development, Frontend Development, Backend Development, Database
Architect, Linux System Administration, IT Security

## Skills & Expertise

| Skill                           | Experience
| ------------------------------- | ----------------------------------
| PHP                             | (Expert, 11 years experience)
| Laravel                         | (Advanced, 3 years experience)
| Angular 10                      | (Intermediate, 2 years experience)
| JavaScript                      | (Advanced, 8 years experience)
| TypeScript                      | (Advanced, 2 years experience)
| Bootstrap                       | (Intermediate, 3 years experience)
| Sass                            | (Intermediate, 4 years experience)
| CSS 3                           | (Advanced, 8 years experience)
| Font Awesome                    | (Intermediate, 2 years experience)
| AJAX API                        | (Advanced, 8 years experience)
| Twilio                          | (Intermediate, 3 years experience)
| Google Cloud Platform           | (Beginner, 2 years experience)
| Amazon SES                      | (Beginner, 2 years experience)
| PHPUnit                         | (Expert, 6 years experience)
| Git                             | (Expert, 8 years experience)
| GitLab CI                       | (Intermediate, 6 years experience)
| Node.js                         | (Beginner, 1 year experience)
| Shell Scripting                 | (Expert, 10 years experience)
| Linux                           | (Expert, 10 years experience)
| PostgreSQL                      | (Intermediate, 8 years experience)
| Docker                          | (Beginner, 1 year experience)
| Ruby                            | (Beginner, 1 year experience)
| Rails 7                         | (Beginner, 1 year experience)
| Ansible                         | (Intermediate, 1 year experience)
| Perl                            | (Advanced, 6 years experience)
| SAML 2.0                        | (Expert, 4 years experience)
| XML                             |
| HTML                            | (Advanced, 8 years experience)
| Network Security                |
| jQuery                          | (Advanced, 7 years experience)
| XML Schema                      | (Intermediate, 1 year experience)
| MySQL                           | (Intermediate, 5 years experience)
| Unix Shell Scripting            | (Expert, 5 years experience)
| Encryption                      |
| System Administration           |
| Linux Server                    |
| Linux System Administration     |
| C

## Experience

### **Information Technology Specialist 4/Application, MN.IT DNR** - Saint Paul, MN

October 2023 - October 2024

* Deployed and managed Ruby on Rails applications on both self-hosted environments and Azure, focusing on application configuration, deployment processes, and ensuring smooth operation and updates across platforms.
* Collaborated on a Scrum team to build an ecommerce site in Ruby on Rails. Contributed to the development, deployment, and ongoing management of the application on both self-hosted and Azure environments, with a focus on application configuration and release processes.
* Integrated software with 3rd-party services including Twilio
* Developed a GIS feature within a Rails application, building a system to retrieve and store maps in the database, supporting efficient geospatial data management.
* Re-implemented an existing site from a Spring-based codebase to a modern Rails application, improving maintainability, performance, and scalability with up-to-date technologies and practices.

#### Projects:

- Minnesota Forestry Burning Permits ([https://apps.dnr.state.mn.us/burning-permits/](https://apps.dnr.state.mn.us/burning-permits/))
- Minnesota DNR Snow Depth and Groomed Trail Conditions ([https://dnr.state.mn.us/snow\_depth/](https://dnr.state.mn.us/snow_depth/))
- Minnesota State Forest Nursery Seedling Storefront (*in development*)

### **Software Engineer, Irish Titan** - Saint Louis Park, MN

March 2022 - March 2023

* Developed, deployed, and upgraded Magento sites
* Deployed scalable sites on Magento Cloud and Adobe Commerce
* Developed and deployed Laravel sites
* Developed and deployed Drupal sites
* Developed and deployed Shopify sites
* Integrated software with 3rd-party services including Yotpo, AWS, Klaviyo

#### Projects:

- FORCE America Magento Storefront ([https://forceamerica.com/](https://forceamerica.com/))
- Haskells Storefront ([https://www.haskells.com/](https://www.haskells.com/))
- Ride Outdoors Magento Storefront ([https://rideoutdoors.com/](https://rideoutdoors.com/))
- Skidoo Outlet Magento Storefront ([https://skidoooutlet.com/](https://skidoooutlet.com/))
- Woodworkers Hardware Magento Storefront ([https://www.wwhardware.com/](https://www.wwhardware.com/))

### **Technical Support Engineer, Nagios, Inc.** - Saint Paul, MN

September 2020 - March 2022 *(1 year)*

* Remotely debugged and diagnosed issues with Nagios products, including LAMP
  stack, and ELK stack (Elasticsearch, Logstash, Kibana) applications
* Managed client relations using written and verbal communication
* Coordinated with 35-member team using SalesForce

### **Software Engineer, G2Planet, Inc.** - Minneapolis, MN

October 2013 - August 2020 *(7 years)*

* Lead development on EventMAX and EventCENTRAL platforms using PHP 7, Laravel,
  PostgreSQL, SAML 2.0, Angular 8, Bootstrap, Docker, and Kubernetes
* Implemented internal single sign-on (SSO) system using SAML 2.0, PHP 7,
  Laravel, PostgreSQL, Angular 8, Bootstrap, Font Awesome, and REST API's
* Coordinated with 17 member team, including training
* Developed sites using Angular 8, Kubernetes, GitLab CI, Laravel 5.3, PHP,
  Docker, Google Cloud Platform, Amazon SES

### **Database Development and Management Specialist at Custom Exteriors of Wisconsin, Inc.** - Appleton, WI

August 2009 - March 2010 *(8 months)*

* Supervised the upgrade and implementation of the company's existing database
* Oversaw the development of a comprehensive intranet-based data entry
  interface for internal company use
* Wrote and revised technical manuals and employee training programs for the
  new software infrastructure

### **APS/Tech Support at Winner, Wixson & Pernitz** - Madison, WI

March 2009 - August 2009 *(6 months)*

* Responsible for data entry of invoices and hand-written work
* Managed a complex database of legal client information and billing records
* Learned and improved the existing accounts payable digital infrastructure
* Built a replacement database in Microsoft Access, with linked tables and a
  Visual Basic user interface

### **Outside Technical Consultant at Custom Exteriors of Wisconsin, Inc.** - Appleton, WI

January 2009 - March 2009 *(3 months)*

* Oversaw the design and launch of a large-scale database to store financial
  and work records for thousands of customers and employees

### **Web Portal Contact Training Specialist at University of Wisconsin-Madison** - Madison, WI

July 2008 - January 2009 *(7 months)*

* Chief designer and developer of a complex website to convey a multitude of
  safety and medical information
* Supervised the design of an intuitive and sleek website layout
* Employed advanced graphic and video presentation techniques to a wide range
  of multimedia for use in a web-based setting
* Used digital photography and image manipulation techniques to convey
  information to a wide audience of hundreds of university workers
* Employed HTML, CSS and object-oriented JavaScript to create an intuitive user
  experience

## Technical Experience

Open Source

* Avid contributor on GitHub @h3xx ([https://github.com/h3xx](https://github.com/h3xx))
* StackOverflow: 10 years contributing, top 2% overall ([https://stackoverflow.com/users/237955](https://stackoverflow.com/users/237955))

## Courses

**Computer Software Engineering**

Dunwoody College of Technology

* C# Application Development
* Intro to XML

**Computer Science**

University of Wisconsin-River Falls

* Data Structures and Algorithms	CSIS237
* Computer Organization and x86 Assembly	CSIS355
* Database Management Systems	CSIS333
* Artificial Intelligence	CSIS451
* Operating Systems	CSIS429

## Education

**University of Wisconsin-River Falls**

Computer Science, 2003 - 2007

**Dunwoody College of Technology**

Computer Software Engineering, 2012

## Interests

Linux, open source, GitHub, social networking, sound engineering


Dan Church

Computer Software Professional

wadfujd4a@mozmail.com

Source: [https://codeberg.org/h3xx/resume](https://codeberg.org/h3xx/resume)
