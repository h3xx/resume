# Environment needed to work around "digital envelope routines::unsupported"
# error. Makes anything-older-than-bleeding-edge Webpack working with current
# NodeJS versions. See https://github.com/webpack/webpack/issues/14532
export NODE_OPTIONS = --openssl-legacy-provider

NG = $(PNPM) exec ng

PNPM = pnpm
PNPM_INSTALL = $(PNPM) install --prefer-frozen-lockfile

all: angular-prod

angular-prod: pnpm
	$(NG) build --prod

deploy: angular-prod
	git checkout -f pages || git checkout --orphan=pages
	git rm -rf .
	git checkout - .gitattributes
	for fn in dist/resume/*; do rm -rf $$(basename $$fn) && mv $$fn . && git add $$(basename $$fn); done
	git commit -m 'Deploy Codeberg pages site'
	git branch --set-upstream-to="$$(git config "branch.main.remote")/pages"; true

pnpm: pnpm-lock.yaml
	$(PNPM_INSTALL)


.PHONY: \
	angular-prod \
	deploy \
	pnpm
